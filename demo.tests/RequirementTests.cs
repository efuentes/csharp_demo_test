﻿using System;
using NUnit.Framework;
using demo.Exceptions;

namespace demo.tests
{
  [TestFixture]
  public class RequirementTests
  {
    [Test]
    public void CanCreateSimpleFunctionalRequirement()
    {
      FunctionalRequirement sut = RequirementFactory.CreateSimpleFunctionalRequirement(new Application(), "US-001", "Add a new Customer", new FunctionalRequirementType());

      Assert.That(sut.application, Is.Not.Null);
      Assert.That(sut.code, Is.EqualTo("US-001"));
      Assert.That(sut.name, Is.EqualTo("Add a new Customer"));
      Assert.That(sut.type, Is.Not.Null);
      Assert.That(sut.status, Is.EqualTo(RequirementStatus.ToBeApproved));
    }

    [Test]
    public void CanNotCreateSimpleFunctionalRequirementWithoutCode()
    {
      Assert.That(() =>
        RequirementFactory.CreateSimpleFunctionalRequirement(new Application(), "", "Add a new Customer", new FunctionalRequirementType()),
        Throws.TypeOf<RequirementIncompleteException>());
    }

    [Test]
    public void CanNotCreateSimpleFunctionalRequirementWithoutName()
    {
      Assert.That(() =>
        RequirementFactory.CreateSimpleFunctionalRequirement(new Application(), "US-001", "", new FunctionalRequirementType()),
        Throws.TypeOf<RequirementIncompleteException>());
    }

    [Test]
    public void CanAddAcceptanceCriteria()
    {
      FunctionalRequirement requirement = RequirementFactory.CreateSimpleFunctionalRequirement(new Application(), "US-001", "Add a new Customer", new FunctionalRequirementType());

      AcceptanceCriteria sut = RequirementFactory.AddAcceptanceCriteria(requirement, "AC-001", "Successful adding", "Given... When... Then...");

      Assert.That(requirement.acceptanceCriterias, Is.Not.Empty);
      Assert.That(sut.code, Is.EqualTo("AC-001"));
      Assert.That(sut.name, Is.EqualTo("Successful adding"));
      Assert.That(sut.description, Is.EqualTo("Given... When... Then..."));
    }

    [Test]
    public void CanNotCreateAcceptanceCriteriaWithoutCode()
    {
      FunctionalRequirement requirement = RequirementFactory.CreateSimpleFunctionalRequirement(new Application(), "US-001", "Add a new Customer", new FunctionalRequirementType());

      Assert.That(() =>
        RequirementFactory.AddAcceptanceCriteria(requirement, "", "Successful adding", "Given... When... Then..."),
        Throws.TypeOf<RequirementIncompleteException>());
    }

    [Test]
    public void CanNotCreateAcceptanceCriteriaWithoutName()
    {
      FunctionalRequirement requirement = RequirementFactory.CreateSimpleFunctionalRequirement(new Application(), "US-001", "Add a new Customer", new FunctionalRequirementType());

      Assert.That(() =>
        RequirementFactory.AddAcceptanceCriteria(requirement, "AC-001", "", "Given... When... Then..."),
        Throws.TypeOf<RequirementIncompleteException>());
    }

    [Test]
    public void CanNotCreateAcceptanceCriteriaWithoutDescription()
    {
      FunctionalRequirement requirement = RequirementFactory.CreateSimpleFunctionalRequirement(new Application(), "US-001", "Add a new Customer", new FunctionalRequirementType());

      Assert.That(() =>
        RequirementFactory.AddAcceptanceCriteria(requirement, "AC-001", "Successful adding", ""),
        Throws.TypeOf<RequirementIncompleteException>());
    }

  }
}
